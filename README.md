## Description
Prototype showcasing typical mechanics from a building game, where you gather resources to build and upgrade buildings.

## Preview
![preview](gameplay.gif)

## Features
* gather different resources over time
    * wood
	* stone
	* food
* place, remove and upgrade different buildings
	* lumberjack (gathers wood)
	* stonemason (gathers stone)
	* farm (gathers food)
	* house
