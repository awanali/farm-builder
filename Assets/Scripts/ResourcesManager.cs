﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourcesManager : MonoBehaviour
{
    public float Wood, Stone, Food;

    public static ResourcesManager Instance;

    [Header("UI Reference")]
    public Text ResourcesText;

    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        ResourcesText.text = $"Resources: Wood: {Wood.ToString("F0")} | Stone: {Stone.ToString("F0")} | Food: {Food.ToString("F0")}";
    }
}
