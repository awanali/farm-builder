﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridElement : MonoBehaviour
{

    public int GridId;

    public bool Occupied;

    public Building ConnectedBuilding;

    [Header("Colors")]
    public Color HoverColor = Color.white;

    public Color OccupiedColor = Color.red;

    public Color SelectedColor = Color.yellow;

    private Color _defaultColor;

    // Start is called before the first frame update
    void Start()
    {

        for (int i = 0; i < BuildingManager.Instance.BuildingGrid.Length; i++)
        {
            if (BuildingManager.Instance.BuildingGrid[i].transform == transform)
            {
                GridId = i;
                break;
            }
        }


        _defaultColor = GetComponent<MeshRenderer>().material.color;
    }

    // Update is called once per frame
    void Update()
    {

        if (BuildingManager.Instance.SelectedGridElement != null && BuildingManager.Instance.SelectedGridElement == this)
        {
            GetComponent<MeshRenderer>().material.color = SelectedColor;
        }
        else if (BuildingManager.Instance.HoveredGridElement == this)
        {
            if (Occupied)
            {
                GetComponent<MeshRenderer>().material.color = OccupiedColor;
            }
            else
            {
                GetComponent<MeshRenderer>().material.color = HoverColor;
            }
        }
        else
        {
            GetComponent<MeshRenderer>().material.color = _defaultColor;
        }
    }
}
