﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyButton : MonoBehaviour
{

    public BuildingType BuildType;

    private Building _connectedBuilding;

    public Text ResourcesText;

    private Button _button;

    // Start is called before the first frame update
    void Start()
    {
        _button = GetComponent<Button>();

        foreach (GameObject buildable in BuildingManager.Instance.Buildables)
        {
            Building building = buildable.GetComponent<Building>();

            if ((building.Info.Type == BuildType))
            {
                _connectedBuilding = building;
                break;
            }
        }

        ResourcesText.text = $"{_connectedBuilding.Price.WoodPrice.ToString("F0")} W. | {_connectedBuilding.Price.StonePrice.ToString("F0")} S. | {_connectedBuilding.Price.FoodPrice.ToString("F0")} F.";
    }

    // Update is called once per frame
    void Update()
    {
        _button.interactable = IsPurchasePossible();
    }

    private bool IsPurchasePossible()
    {
        if (ResourcesManager.Instance.Wood >= _connectedBuilding.Price.WoodPrice
                && ResourcesManager.Instance.Stone >= _connectedBuilding.Price.StonePrice
                && ResourcesManager.Instance.Food >= _connectedBuilding.Price.FoodPrice)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
