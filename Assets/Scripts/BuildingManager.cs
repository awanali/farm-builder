﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingManager : MonoBehaviour
{
    public static BuildingManager Instance;

    public List<GameObject> Buildables = new List<GameObject>();

    [HideInInspector]
    public List<GameObject> BuiltObjects = new List<GameObject>();

    [HideInInspector]
    public GridElement SelectedGridElement, HoveredGridElement;

    public GridElement[] BuildingGrid;

    private bool _isBuilding;

    private GameObject _buildableObject;

    private RaycastHit _mouseHit;

    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out _mouseHit))
        {
            GridElement gridElement = _mouseHit.transform.GetComponent<GridElement>();

            if (!gridElement)
            {
                return;
            }

            if (Input.GetMouseButtonUp(0))
            {
                SelectedGridElement = gridElement;
                PlaceBuilding();
            }

            HoveredGridElement = gridElement;

        }

        MoveBuilding();
    }

    public void CreateBuilding(int type)
    {

        if (_isBuilding)
        {
            return;
        }

        foreach (GameObject gameObject in Buildables)
        {
            Building building = gameObject.GetComponent<Building>();

            if ((building.Info.Type == (BuildingType)type))
            {
                _buildableObject = Instantiate(building.gameObject);
                break;
            }
        }

        _isBuilding = true;

    }

    public void MoveBuilding()
    {
        if (!_buildableObject)
        {
            return;
        }

        _buildableObject.gameObject.layer = 2;

        if (HoveredGridElement)
        {
            _buildableObject.transform.position = HoveredGridElement.transform.position;
        }

        if (Input.GetMouseButton(1))
        {
            Destroy(_buildableObject);
            _buildableObject = null;
            _isBuilding = false;
        }

        if (Input.GetMouseButton(2))
        {
            _buildableObject.transform.Rotate(transform.up * 5);
        }
    }

    public void PlaceBuilding()
    {
        if (!_buildableObject || HoveredGridElement.Occupied)
        {
            return;
        }


        BuiltObjects.Add(_buildableObject);
        HoveredGridElement.Occupied = true;

        Building building = _buildableObject.GetComponent<Building>();
        HoveredGridElement.ConnectedBuilding = building;

        building.Placed = true;

        building.UpgradeBuilding();

        building.Info.ConnectedGridId = HoveredGridElement.GridId;
        building.Info.YRot = building.transform.localEulerAngles.y;

        _buildableObject = null;
        _isBuilding = false;

    }
}
