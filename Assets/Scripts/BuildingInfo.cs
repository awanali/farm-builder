﻿
[System.Serializable]
public enum BuildingType
{
    Lumberjack,
    Stonemason,
    Farm,
    House
}

[System.Serializable]
public class BuildingInfo
{
    public BuildingType Type;

    public float Level = 0;

    public float YRot = 0;

    public int ConnectedGridId;
}
