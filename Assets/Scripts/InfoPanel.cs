﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoPanel : MonoBehaviour
{

    public Text NameText;

    public Button DestroyButton;

    public Button UpgradeButton;

    private Building _selectedBuilding;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (BuildingManager.Instance.SelectedGridElement != null && BuildingManager.Instance.SelectedGridElement.ConnectedBuilding != null)
        {
            _selectedBuilding = BuildingManager.Instance.SelectedGridElement.ConnectedBuilding;
            NameText.text = _selectedBuilding.ObjectName + "\nLevel: " + _selectedBuilding.Info.Level;
        }
        else
        {
            NameText.text = "No building selected";
            _selectedBuilding = null;
        }

        DestroyButton.interactable = _selectedBuilding;

        UpgradeButton.interactable = IsUpgradePossible();


    }

    private bool IsUpgradePossible()
    {
        if (!_selectedBuilding)
        {
            return false;
        }
        
        if (ResourcesManager.Instance.Wood >= _selectedBuilding.Price.WoodPrice
                && ResourcesManager.Instance.Stone >= _selectedBuilding.Price.StonePrice
                && ResourcesManager.Instance.Food >= _selectedBuilding.Price.FoodPrice)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void OnButtonUpgrade()
    {
        if (_selectedBuilding)
        {
            _selectedBuilding.UpgradeBuilding();
        }
    }

    public void OnButtonDestroy()
    {
        if (_selectedBuilding)
        {
            BuildingManager.Instance.SelectedGridElement.Occupied = false;
            BuildingManager.Instance.BuiltObjects.Remove(_selectedBuilding.gameObject);
            Destroy(_selectedBuilding.gameObject);
        }
    }
}
