﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{

    public BuildingInfo Info;

    public string ObjectName;

    [HideInInspector]
    public bool Placed;

    public PriceTag Price;

    public int BaseResourceGain = 1;

    // Start is calle[HideInInspector]d before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!Placed)
        {
            return;
        }

        switch (Info.Type)
        {
            case BuildingType.Lumberjack:
                ResourcesManager.Instance.Wood += (BaseResourceGain * Info.Level) * Time.deltaTime;
                break;
            case BuildingType.Stonemason:
                ResourcesManager.Instance.Stone += (BaseResourceGain * Info.Level) * Time.deltaTime;
                break;
            case BuildingType.Farm:
                ResourcesManager.Instance.Food += (BaseResourceGain * Info.Level) * Time.deltaTime;
                break;
        }
    }

    public void UpgradeBuilding()
    {
        Info.Level++;

        ResourcesManager.Instance.Wood -= Price.WoodPrice;
        ResourcesManager.Instance.Stone -= Price.StonePrice;
        ResourcesManager.Instance.Food -= Price.FoodPrice;
    }
}
